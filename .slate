##########################
##Slate Configuration Notes###
#########################
#
# Initially based on slate configuration file: https://github.com/jigish/slate
#then incorporated material from http://thume.ca/howto/2012/11/19/using-slate/
#and further customized

#full documentation here: https://github.com/jigish/slate

########################################
# COMMANDS SANDBOX -- commands being worked on but not ready for prime-time yet

alias mon-dell24    1920x1200
alias 1-chat  move screenOriginX;screenOriginY	screenSizeX/9;screenSizeY ${mon-dell24}
bind pad4:ctrl ${1-chat}


########################################

#TO DO----For future customization: 
# need to figure how windows can be moved to percentage of screen or relative positions
#construct specific layouts for laptop, Dell and Thunderbolt Displays
#Can you have bindings that are specific to layouts (i.e external keyboard vs. internal)
#see if you can use 'default' directive to trigger display specific layouts
#need to check further into 'snapshots' feature

#####################
###Begin Configuration###
#####################

########################################
# Highlevel CONFIG options
# see https://github.com/jigish/slate/wiki/Global-Configs
########################################

config defaultToCurrentScreen true   
config nudgePercentOf screenSize
config resizePercentOf screenSize
config windowHintsShowIcons true
config windowHintsIgnoreHiddenWindows false
config windowHintsSpread true


##############################################
# ALIASING 
# see: https://github.com/jigish/slate#the-alias-directive
############################################

# Monitor Aliases
alias mon-mbpro      1280x800
alias mon-thunderbolt 2560x1440
alias mon-dell24    1920x1200
alias mon-mbair 1366x768

#Abstract Position Aliases
alias full move screenOriginX;screenOriginY screenSizeX;screenSizeY
alias lefthalf move screenOriginX;screenOriginY screenSizeX/2;screenSizeY
alias righthalf move screenOriginX+screenSizeX/2;screenOriginY screenSizeX/2;screenSizeY
alias tophalf move screenOriginX;screenOriginY screenSizeX;screenSizeY/2
alias bottomhalf move screenOriginX;screenOriginY+screenSizeY/2 screenSizeX;screenSizeY/2
alias topleft corner top-left resize:screenSizeX/2;screenSizeY/2
alias topright corner top-right resize:screenSizeX/2;screenSizeY/2
alias bottomleft corner bottom-left resize:screenSizeX/2;screenSizeY/2
alias bottomright corner bottom-right resize:screenSizeX/2;screenSizeY/2

#used in goodmorning layout
alias termtop corner top-right resize:3*screenSizeX/12;3*screenSizeY/10
alias caltop corner top-left resize:4*screenSizeX/12;3*screenSizeY/10
alias txtbottom corner bottom-left resize:2*screenSizeX/12;4*screenSizeY/10
alias mailbottom corner bottom-right resize:4*screenSizeX/12;7*screenSizeY/10

#####################################################
# BINDING WINDOW LOCATIONS::The following 'bind' lines allow you to hold "CTRL" and "COMMAND"
# and then press a third key that corresponds to 9 sections of a
# square:
#
#   u  i  o
#   j  k  l
#   m  ,  .
#
#see: https://github.com/jigish/slate#the-bind-directive
###################################################

# Grid to use for repositioning/resizing windows- various resolutions by monitors

bind 3:ctrl grid padding:5 1920x1200:8,12 1280x800:8,10 2560x1440:12,10 1366 x 768:12,10

#Binding Abstract Locations

bind u:ctrl;cmd  ${topleft}
bind j:ctrl;cmd  ${lefthalf}
bind m:ctrl;cmd  ${bottomleft}

bind i:ctrl;cmd  ${tophalf}
bind k:ctrl;cmd  ${full}
bind ,:ctrl;cmd  ${bottomhalf}

bind o:ctrl;cmd  ${topright}
bind l:ctrl;cmd  ${righthalf}
bind .:ctrl;cmd  ${bottomright}

#####################################################
# CYCLING THROUGH OPEN APPS
#
#will cause Slate to display the letters on the
# home row above window. Logos will appear on these letters, and you
# can press the letter to switch to that window.
######################################################

#Hint screen for open windows
bind esc:cmd hint ASDFGHJKL

#################################
##LAYOUTS
#see:https://github.com/jigish/slate#the-layout-directive
#################################


##Goodmorning (layout to run with alfred workflow keycommand 'morning')
layout morning 'Terminal' ${termtop}
layout morning 'Calendar' ${caltop}
layout morning 'Mail' ${mailbottom}
layout morning 'TodoTxtMac' ${txtbottom}
bind pad0:ctrl;cmd layout morning

##Prioritized Layout -- Layout that moves windows to priority positions if open
layout 1monitor 'Terminal':REPEAT ${termtop}
layout 1monitor 'Sublime Text':REPEAT ${lefthalf}
layout 1monitor 'Mail':REPEAT ${bottomhalf}
layout 1monitor 'Spotify':REPEAT ${full}
bind /:ctrl;cmd layout 1monitor

############################################################
##SNAPSHOTS
#see: https://github.com/jigish/slate#snapshot
#################################
# name = the name of the snapshot to create (used in delete-snapshot and activate-snapshot)
#options = (optional) a semicolon separated list of any of the following options:
# save-to-disk -> saves the snapshot to disk so Slate will load it when it starts up next
# stack -> treats this snapshot as stack so you can use this binding multiple times to push snapshots on the stack
##############################################################

#CREATE snapshot: Will bind the keystroke ctrl+cmd-home to create a snapshot called `mysnapshot`, save that
#snapshot to disk, and treat it as a stack so you can hit the keystroke multiple times to push snapshots onto the stack.

bind home:shift;ctrl;cmd snapshot mysnapshot save-to-disk;stack

#ACTIVATE snapshot: activates this snapshot

bind home:ctrl;cmd activate-snapshot mysnapshot

#DELETE snapshot: deletes complete snapshot 

bind delete:shift;ctrl;cmd delete-snapshot mysnapshot all

##########################################################
##MULTI Display Bindings#
###############################
# These bindings make it so that you can more easily move windows to
# other displays. If you have two displays, you can press CTRL + CMD +
# 2 to cause the current window to be moved, and appear full screen,
# on your second display. If you subsequently press CTRL + CMD + 1,
# the window which was just moved to your second display will come
# back to your first display
#######################################################

bind 1:ctrl;cmd         throw 0 resize
bind 2:ctrl;cmd         throw 1 resize
bind 3:ctrl;cmd         throw 2 resize
